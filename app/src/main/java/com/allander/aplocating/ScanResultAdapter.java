package com.allander.aplocating;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by johan on 2019-01-14.
 */

public class ScanResultAdapter extends ArrayAdapter<ScanResult> {
    Context context;
    private List<ScanResult> scanResultList;

    public ScanResultAdapter(@NonNull Context context, List<ScanResult> list) {
        super(context, 0, list);
        this.context = context;
        this.scanResultList = list;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem==null)
            listItem = LayoutInflater.from(this.context).inflate(R.layout.scanitem,parent,false);

        ScanResult currentScanResult = scanResultList.get(position);


        TextView SSIDText = listItem.findViewById(R.id.SSIDText);
        TextView BSSIDText = listItem.findViewById(R.id.BSSIDText);
        TextView RSSIText = listItem.findViewById(R.id.RSSIText);
        TextView realCoord = listItem.findViewById(R.id.coordText);

        BSSIDText.setText(currentScanResult.BSSID);
        SSIDText.setText(currentScanResult.SSID);
        RSSIText.setText("[ " + currentScanResult.level +" meter ]");
        realCoord.setText(currentScanResult.capabilities);

        return listItem;
    }
}
