package com.allander.aplocating;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.widget.TextView;

/**
 * Created by johan on 2019-01-16.
 */

public class AsyncUpdateText {
    static private Handler handler = null;

    AsyncUpdateText(){}

    static public void init(Context context) {
        handler = new Handler(context.getMainLooper());
    }

    public void updateUI(TextView destination, String output) {
        if(handler == null) {
            Log.d("GPS","Cant use updateUI before it's been initialized! Run init()");
            return;
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                destination.setText(output);
            }
        });
    }

}