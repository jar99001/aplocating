/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allander.aplocating;

import java.text.DecimalFormat;

/**
 *
 * @author johan
 */
public class Coord {
    Double x;
    Double y;
    
    Coord(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }
    
    @Override
    public String toString() {
        if(x.isNaN() || y.isNaN()) return "Lat: X.X, Long: X.X";
        DecimalFormat numberFormat = new DecimalFormat("#.00");
        return "Lat: " + x + "\nLong:" + y;
//        return "Lat: " + numberFormat.format(x) + ", Long:" + numberFormat.format(y);
    }

}
