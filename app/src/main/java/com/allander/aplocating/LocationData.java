package com.allander.aplocating;

import java.io.Serializable;

/**
 * Created by johan on 2019-01-15.
 */

public class LocationData  {
    Double latitude, longitude,level;
    static Integer dataColledted = 0;

    public LocationData(Double latitude, Double longitude, Double level) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.level = level;
        dataColledted++;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLevel() {
        return level;
    }

    public void setLevel(Double level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "Lat: " + this.latitude + ", Long: " + this.longitude + ", Pwr: " + this.level + "\n";
    }

    public static Integer getDataColledted() {
        return dataColledted;
    }
}
