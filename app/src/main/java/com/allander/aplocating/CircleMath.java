/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.allander.aplocating;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author johan
 */
public class CircleMath {
    Double X1,X2,Y1,Y2;

    
    CircleMath(){}
    
       public static Coord bestLocationAP(List<Coord> list) {
        Coord base = pointInTheMiddle(list);
        List<Double> distList = new ArrayList<>();

        list.forEach(b -> distList.add(lengthBetweenCoords(base,b)));

//        list.forEach(System.out::println);

        Double sum = 0.0;
        for(Double k : distList) {
            sum += k;
        };
        Double middle = sum/distList.size();
//        System.out.println(list.size());
        Integer removed = 0;
        for(int a=0;a<distList.size();a++) {
//            System.out.println(distList.get(a));
            if(distList.get(a) > middle) list.remove(a - removed++);
        }
        
        list.forEach(System.out::println);

//      Coord minCoord = listCoords.stream().max(Comparator.comparing(Coord::get))
//      .stream()
//      .min(Comparator.comparing(Person::getAge))
//      .orElseThrow(NoSuchElementException::new);
          
        return pointInTheMiddle(list);
    }

    
    public static Coord pointInTheMiddle(List<Coord> list) {
        Double  x = 0.0,
                y = 0.0;
        Integer count = 0;

        for(Coord k : list) {
            x += k.getX();
            y += k.getY();
            count++;
        }
        
        return new Coord(x/count,y/count);
    }
    
    public Coord calcCrossection(LocationData pos1, LocationData pos2, Boolean firstRoot) {
        // Points are the same, return null
        if(pos1.getLatitude() == pos2.getLatitude() && pos1.getLongitude() == pos2.getLongitude()) return null;

        // Circles dont intersect, return null
        Double length = lengthBetweenCoords(new Coord(pos1.getLatitude(),pos1.getLongitude()),new Coord(pos2.getLatitude(),pos2.getLongitude()));
        if(pos1.level/10 + pos2.level/10 < length) return null;

        Double 
                a = pos1.getLatitude(),
                b = pos1.getLongitude(),
                c = pos2.getLatitude(),
                d = pos2.getLongitude(),
                r0 = pos1.getLevel().doubleValue()/10,
                r1 = pos2.getLevel().doubleValue()/10,
//                a = 4.0,
//                b = 5.0,
//                c = 7.0,
//                d = 7.0,
//               r0 = 2.0,
//               r1 = 2.5,
                D = Math.sqrt((c-a)*(c-a) + (d-b)*(d-b)), // Rätt
                O = Math.sqrt((D+r0+r1)*(D+r0-r1)*(D-r0+r1)*(-D+r0+r1))/4, // Rätt
                mX = (a+c)/2 + ((c-a)*(r0*r0 - r1*r1))/(2*D*D),
                mY = (b+d)/2 + ((d-b)*(r0*r0 - r1*r1))/(2*D*D),
                V1 = (2*(b-d)*O)/(D*D),
                V2 = (2*(a-c)*O)/(D*D);
        
        X1 = mX + V1;
        X2 = mX - V1;
        Y1 = mY - V2;
        Y2 = mY + V2;

        
//        System.out.println("D: " + D + ", O: " + O);
//        System.out.println("X1: " + X1 + ", Y1: " + Y1);
//        System.out.println("X2: " + X2 + ", Y2: " + Y2);

        if(firstRoot) {
            return new Coord(X1,Y1);
        } else {
            return new Coord(X2,Y2);
        }
    }
    
    public static Double lengthBetweenCoords(Coord a, Coord b) {
        return Math.sqrt((b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y - a.y));
    }

    static public List<Coord> addCoordsFromLocationDataList(List<LocationData> list) {
        CircleMath c = new CircleMath();

        List<Coord> coordList = new ArrayList<>();
        for(LocationData l : list) {
            l.level /= 43496.0;
            Log.d("GPS","Length[" + l.level*43496 + "], decimal degrees: " + l.level);
        }

        for(int a=0;a<list.size();a++) {
            for(int b=a+1;b<list.size();b++) {
                coordList.add(c.calcCrossection(list.get(a), list.get(b), true));
                coordList.add(c.calcCrossection(list.get(a), list.get(b), false));
            }
        }

        coordList.removeAll(Collections.singleton(null));
        return coordList;
    }

    @Override
    public String toString() {
        return "X1:" + X1 + ", Y1:" + Y1 + "X2:" + X2 + ", Y2:" + Y2;
    }
    
}
