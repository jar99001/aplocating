package com.allander.aplocating;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Parcel;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    LocationManager lm;
    MyLocationListener gpsListener;
    Looper looper;
    HandlerThread handlerThread;
    Context context = this;
    Boolean newWifiResults = false;
    WifiManager wifiManager;
    BroadcastReceiver wifiReciver;
    ScanResultAdapter scanResultAdapter;
    ListView listView;
    HashMap<String, List<LocationData>> APData = new HashMap<>();
    IntentFilter intentFilter;
    TextView dataPointsText;
    LocationData dataPointCounter;
    AsyncUpdateText asyncUpdateText;


    public Double calculateDistance(Integer signalLevelInDb, double freqInMHz) {
        double exp = (27.55 - (20 * Math.log10(freqInMHz)) + Math.abs(signalLevelInDb.doubleValue())) / 20.0;
        return Math.pow(10.0, exp);
    }


    private void newWifiInfo() {
        List<ScanResult> results = wifiManager.getScanResults();

        ((Runnable) () -> {
            if (gpsListener.getNewScan()) {
                for (ScanResult s : results) {

                    s.level = calculateDistance(s.level,s.frequency).intValue();
                    APData.computeIfAbsent(s.BSSID, k -> new ArrayList<>()).add(new LocationData(gpsListener.getLatitude(), gpsListener.getLongitude(), (double) s.level));
                }
                Log.d("GPS", "Collecting new data");
                gpsListener.setNewScan(false);
            } else {
                Log.d("GPS", "Showing new data");
            }
            //        Log.d("GPS", APData.toString());
            updateUI(dataPointsText, dataPointCounter.getDataColledted().toString());
            results.sort(Comparator.comparingDouble(o -> o.level));

            for (ScanResult s : results) {
                if (APData.containsKey(s.BSSID)) {
                    List<LocationData> tList = APData.get(s.BSSID);
                    Coord realCoord = calculatePositions(tList);
                    s.capabilities = realCoord.toString();
                } else {
                    s.capabilities = "";
                }
            }
        /*
                        // Calculate positions
                        for(Map.Entry<String, List<LocationData>> entry : APData.entrySet()) {
                            Log.d("GPS",entry.getKey());
                            Coord realCoord = calculatePositions(entry.getValue());
                            Log.d("GPS","Real coords: " + realCoord);
                        }
        */
            scanResultAdapter = new ScanResultAdapter(context, results);
            listView.setAdapter(scanResultAdapter);

        }).run();

    }

    private Coord calculatePositions(List<LocationData> apData) {
            CircleMath c = new CircleMath();
            List<Coord> coordList = c.addCoordsFromLocationDataList(apData);
            Coord realCoords = CircleMath.bestLocationAP(coordList);
//            Log.d("GPS"," Real:[ " + realCoords + "]");
            return realCoords;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AsyncUpdateText.init(context);

        handlerThread = new HandlerThread("MyHandlerThread");
        handlerThread.start();
        looper = handlerThread.getLooper();

        dataPointCounter = new LocationData(1.0,1.0,0.0);


        Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        final Button doItButton = findViewById(R.id.doItButton);
        final Button exitButton = findViewById(R.id.exitButton);
        final Button printDataButton = findViewById(R.id.printDataButton);
        final TextView infoText = findViewById(R.id.infoText);
        dataPointsText = findViewById(R.id.dataPointsValue);
        listView = findViewById(R.id.scanList);


        ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_WIFI_STATE,
                Manifest.permission.CHANGE_WIFI_STATE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.VIBRATE}, 1);


        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

        wifiReciver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                newWifiResults = intent.getBooleanExtra(WifiManager.EXTRA_RESULTS_UPDATED, false);
                if (newWifiResults && gpsListener.getNewScan()) {
                    newWifiInfo();
                }
            }
        };

        intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        context.registerReceiver(wifiReciver, intentFilter);


        doItButton.setOnClickListener((View v) -> {
            vib.vibrate(VibrationEffect.createOneShot(200,VibrationEffect.DEFAULT_AMPLITUDE));

            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d("GPS", "Error: Cant access\ngps location listener.");
            } else {
                lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                gpsListener = new MyLocationListener(wifiManager, infoText);
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, gpsListener, looper);
                lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, gpsListener, looper);
            }
        });

        exitButton.setOnClickListener(v -> {
            vib.vibrate(VibrationEffect.createWaveform(new long[]{0L,200L,200L,200L},-1));
            writeToFile(APData.toString(), context);
            if (gpsListener != null && lm != null) lm.removeUpdates(gpsListener);
            context.unregisterReceiver(wifiReciver);
            finishAffinity();
        });

        printDataButton.setOnClickListener(v -> {
            vib.vibrate(VibrationEffect.createOneShot(200,VibrationEffect.DEFAULT_AMPLITUDE));
            for(Map.Entry<String, List<LocationData>> entry : APData.entrySet()) {
                Log.d("GPS",entry.getKey());
                Coord realCoord = calculatePositions(entry.getValue());
                Log.d("GPS","[Data: " + entry.getValue().size() + " ]Real coords: " + realCoord);
                for(LocationData l : entry.getValue()) {
                    Log.d("GPS",l.toString());
                }
            }
        });

    }

    // Lånad funktion
    private void writeToFile(String data,Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("ApGpsData.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public void updateUI(TextView dest, String value) {
        Runnable task = new Runnable() {
            @Override
            public void run() {
                dest.setText(value);
            }
        };
        task.run();
    }

}
