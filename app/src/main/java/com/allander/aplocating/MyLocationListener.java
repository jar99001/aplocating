package com.allander.aplocating;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by johan on 2019-01-14.
 */

public class MyLocationListener  implements LocationListener {
    Double latitude, longitude;
    Boolean anyValue = false;
    WifiManager wifiManager;
    Location oldLocation;
    TextView infoText;
    AsyncUpdateText screen = new AsyncUpdateText();
    Boolean newScan = false;
//    Context context;


    public MyLocationListener(WifiManager wifiManager, TextView infoText) {
        this.infoText = infoText;
        this.wifiManager = wifiManager;
    }

    @Override
    public void onLocationChanged(Location location) {
//        infoText.setText("Waiting for better accuracy...");
        screen.updateUI(infoText,"Waiting for better accuracy...");
        Log.d("GPS", "Lat: " + location.getLatitude()+ ", Long: " + location.getLongitude() + " +- " + location.getAccuracy());

        if(location.getAccuracy()<30) {
            screen.updateUI(infoText,"Walk around to get more data...");
            if(!anyValue || location.distanceTo(oldLocation) > 5.0) {
                if(anyValue) {
                    Log.d("GPS", "Collecting data, dist: " + location.distanceTo(oldLocation));
                    screen.updateUI(infoText,"Collecting data, dist: " + location.distanceTo(oldLocation));
                }
                this.latitude = location.getLatitude();
                this.longitude = location.getLongitude();
                this.anyValue = true;
                oldLocation = location;
                newScan = true;
                wifiManager.startScan();
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("GPS","On Status Changed");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("GPS","On Provider Enabled");
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("GPS","On Provider Disabled");
    }

    public Double getLatitude() {
        if(anyValue) return latitude;
        else return null;
    }

    public Double getLongitude() {
        if(anyValue) return longitude;
        else return null;
    }

    public Boolean getNewScan() {
        return newScan;
    }

    public void setNewScan(Boolean newScan) {
        this.newScan = newScan;
    }


}
